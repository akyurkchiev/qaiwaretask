/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.message.server.service;

import com.message.server.domain.MessageHistory;
import java.util.List;

/**
 *
 * @author Nacho
 */
public interface MessageService {
    
    List<MessageHistory> getAllMessages();    
    void postMessage(String type, String paylod);
    
}
