CREATE DATABASE  IF NOT EXISTS `messageserver` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `messageserver`;

DROP TABLE IF EXISTS `message_history`;

CREATE TABLE `message_history` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `type` varchar(255) DEFAULT NULL,
  `payload` varchar(255) DEFAULT NULL,  
  `created_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;