package com.message.server.controller;

import static org.hamcrest.Matchers.equalTo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 *
 * @author Nacho
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class MessageControllerTest {

    private static final String DEFAULT_URI = "/messages/";

    @Autowired
    private MockMvc mvc;

    @InjectMocks
    MessageController controller;

    /**
     * Positive test to check if the message with type send_text is correct proceed
     * @throws Exception 
     */
    @Test
    public void sendTextWithSuccess() throws Exception {
        String payload = "Nasko i Bela";

        mvc.perform(MockMvcRequestBuilders.post(DEFAULT_URI + "send_text").content(payload)
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(content().string(equalTo("")));
    }

    /**
     * Positive test to check if the message with type send_emotion is correct proceed
     * The test check also the length of the given text
     * @throws Exception 
     */
    @Test
    public void sendEmojiWithSuccess() throws Exception {
        String payload = "Nasko";
        mvc.perform(MockMvcRequestBuilders.post(DEFAULT_URI + "send_emotion").content(payload)
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(content().string(equalTo("")));

    }
    /**
     * Negative test to check if the given type is not send_text or send_emotion
     * The test check also the length and if the given string contains numeric characters
     * @throws Exception 
     */
    @Test
    public void checkIfTypeIsCorrect() throws Exception {
        String payload = "Nasko";
        mvc.perform(MockMvcRequestBuilders.post(DEFAULT_URI + "send").content(payload)
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(content().string(equalTo("")));
    }
        /**
     * Negative test to check if the received message with type send_text has the correct length
     * @throws Exception 
     */
    @Test
    public void negativeCheckSendTextLength() throws Exception {
        String payload = "ManchesterUnitedManchesterUnitedManchesterUnitedManchesterUnitedManchesterUnitedManchesterUnitedManchesterUnitedManchesterUnitedManchesterUnitedManchesterUnitedManchesterUnited";
        mvc.perform(MockMvcRequestBuilders.post(DEFAULT_URI + "send_text").content(payload)
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(content().string(equalTo("")));

    }
    /**
     * Negative test to check the if message with type send_emotion has the correct length and contains numeric characters
     * @throws Exception 
     */
    @Test
    public void checkSendEmojiLengthAndIfContainsDigits() throws Exception {
        String payload = "nasko1234";
        mvc.perform(MockMvcRequestBuilders.post(DEFAULT_URI + "send_emoji").content(payload)
                .accept(MediaType.APPLICATION_JSON)).andExpect(status().isCreated())
                .andExpect(content().string(equalTo("")));

    }
}
