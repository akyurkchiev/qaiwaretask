/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.message.server.repository;

import com.message.server.domain.MessageHistory;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Nacho
 */
@Repository
public interface MessageRepo extends JpaRepository<MessageHistory, Long> {
    
}
