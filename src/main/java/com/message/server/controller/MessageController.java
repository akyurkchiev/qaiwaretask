package com.message.server.controller;

import com.message.server.service.MessageService;
import javax.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Nacho
 */
@RestController
@RequestMapping("/messages")
public class MessageController {

    public static final Logger LOG = LoggerFactory.getLogger(MessageController.class);

    @Autowired
    MessageService messageService;

    public void setService(MessageService service) {
        this.messageService = service;
    }

//    @RequestMapping(method = RequestMethod.GET)
//    public List<MessageHistory> findAll() {
//        return messageService.getAllMessages();
//    }
    /**
     *
     * @param type
     * @param payload
     * @param response
     * @throws Exception
     */
    @RequestMapping(method = RequestMethod.POST, value = "/{type}")
    public void receiveMessage(@PathVariable String type, @RequestBody String payload, HttpServletResponse response) throws Exception {
//        validatePayload(type, payload);
        if ("send_text".equals(type)) {
            if (payload.length() < 1 || payload.length() > 160) {
                response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
            } else {
                messageService.postMessage(type, payload);
                response.setStatus(HttpServletResponse.SC_CREATED);
            }
        } else if ("send_emotion".equals(type)) {
            if (payload.length() < 2 || payload.length() > 10 || containsDigit(payload)) {
                response.setStatus(HttpServletResponse.SC_PRECONDITION_FAILED);
            } else {
                messageService.postMessage(type, payload);
                response.setStatus(HttpServletResponse.SC_CREATED);
            }
        } else {
            response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
            throw new Exception("Incorect type in the URL! " + type);
        }
    }

    /**
     * Check if string contains digits [0-9]
     */
    public final boolean containsDigit(String digitString) {
        boolean containsDigit = false;

        if (digitString != null && !digitString.isEmpty()) {
            for (char c : digitString.toCharArray()) {
                if (containsDigit = Character.isDigit(c)) {
                    break;
                }
            }
        }

        return containsDigit;
    }
}
