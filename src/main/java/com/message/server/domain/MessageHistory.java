package com.message.server.domain;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author Nacho
 */
@Entity
@Table(name = "message_history")
public class MessageHistory {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id", nullable = false, updatable = false)
    private Long Id;

    @Column(name = "type")
    private String type;

    @Column(name = "payload")
    private String payload;

    @Column(name = "created_at")
    private Date created;

    public MessageHistory() {
    }

    public MessageHistory(Long Id, String msgType, String payload, Date created) {
        this.Id = Id;
        this.type = msgType;
        this.payload = payload;
        this.created = created;
    }

    public Long getId() {
        return Id;
    }

    public void setId(Long Id) {
        this.Id = Id;
    }

    public String getMsgType() {
        return type;
    }

    public void setMsgType(String msgType) {
        this.type = msgType;
    }

    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

}
