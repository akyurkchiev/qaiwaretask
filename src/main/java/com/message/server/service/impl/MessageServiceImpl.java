package com.message.server.service.impl;

import com.message.server.domain.MessageHistory;
import com.message.server.repository.MessageRepo;
import com.message.server.service.MessageService;
import java.util.Date;
import java.util.List;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 *
 * @author Nacho
 */
@Service
@Transactional
public class MessageServiceImpl implements MessageService {
    
    @Autowired
    private MessageRepo repo;

    public void setRepo(MessageRepo repo) {
        this.repo = repo;
    }
    /*
    * Get all existing messages
    */
    @Override
    public List<MessageHistory> getAllMessages() {
        return repo.findAll();
    }
    
    /**
     * 
     * @param type
     * @param paylod 
     */
    @Override
    public void postMessage(String type, String paylod) {
        MessageHistory historyMsg = new MessageHistory();
        historyMsg.setMsgType(type);
        historyMsg.setPayload(paylod);
        historyMsg.setCreated(new Date());
        
        repo.save(historyMsg);
    }


}
